//
//  Unit.swift
//  Track Pro
//
//  Created by admin on 24/10/20.
//

import Foundation

enum Unit: String, CaseIterable {
    case kilometersPerHour = "km/h"
    case milesPerHour = "m/h"
    
    private static var `default`: Self { //set default unit based on location
        Locale.current.usesMetricSystem ? .kilometersPerHour : .milesPerHour
    }
    
    
    var next: Self {
        let units = Unit.allCases
        
        let index = units.firstIndex(of: self)! + 1
        let unit = (index < units.count) ? units[index] : units.first!
        
        return unit
    }
    
    func calculateSpeed(for speedProvidedByDevice: Double) -> String {
        // There is a minimum of 0.5 m/s needed before the app will return a
        // calculated speed.
        guard speedProvidedByDevice > 0.5 else {
            return "0"
        }
        return String(format: "%.0f", speedProvidedByDevice * 3.6)
        
    }
    
    func calculateSpeedInFloat(for speedProvidedByDevice: Double) -> Float {
        // There is a minimum of 0.5 m/s needed before the app will return a
        // calculated speed.
        guard speedProvidedByDevice > 0.5 else {
            return 0
        }
        return Float(speedProvidedByDevice * 3.6)
        
    }
    
    
    func localizedString(for speed: Double) -> String {
        
        return String(format: "%.0f", speed)
    }
}
