//
//  ActivitiesViewController.swift
//  Track Pro
//
//  Created by admin on 24/10/20.
//

import UIKit
import CoreData
class ActivitiesViewController: UIViewController {

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var avgPaceLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var bgImageView: UIImageView!

    private var activities = [Activities]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchActivitiesFromDB), name: Notification.Name(Constant.identifiers.dbUpdateObserver), object: nil)
       
        fetchActivitiesFromDB()

        self.table.register(UINib(nibName: Constant.identifiers.ActivityListCell, bundle: nil), forCellReuseIdentifier: Constant.identifiers.ActivityListCell)
        self.table.tableFooterView = UIView()
        self.table.dataSource = self
        self.table.delegate = self
    }
    func initUI(){
        
        if let activity = activities.first {
            distanceLabel.text =  activity.distance!
            timeLabel.text = FormatDisplay.time(Int(activity.duration) )
            avgPaceLabel.text = activity.avgSpeed! + " kph"
            modeLabel.text = activity.isCycling  ? Constant.Constants.cycling : Constant.Constants.jogging
            bgImageView.image = activity.isCycling  ? Design.Image.cyclingBG : Design.Image.joggingBG
        }
    }
    

    //MARK:- Fetch from DB
   @objc func fetchActivitiesFromDB() {
        let request: NSFetchRequest<Activities> = Activities.fetchRequest()
        let sort = NSSortDescriptor(key: "timestamp", ascending: false)
        request.sortDescriptors = [sort]

        do {
            activities = try CoreDataStack.context.fetch(request)
            print("in home i got \(activities.count) activities")
            if activities.count == 0 {
                table.tableHeaderView?.isHidden = true
            }else{
                table.tableHeaderView?.isHidden = false
                self.initUI()
                self.table.reloadData()
            }
        } catch {
            print("Fetch failed")
        }
    }

}
//MARK:- Table view operations
extension ActivitiesViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.activities.isEmpty{
            self.table.isHidden = true
        }else{
            self.table.isHidden = false
        }
        return self.activities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.table.dequeueReusableCell(withIdentifier: Constant.identifiers.ActivityListCell) as! ActivityListCell
        cell.distanceLabel.text =  activities[indexPath.row].distance! + "km"
        cell.timeLabel.text = FormatDisplay.time(Int(activities[indexPath.row].duration) )
        cell.avgPaceLabel.text = activities[indexPath.row].avgSpeed! + "kph"
        cell.activityDateLabel.text = FormatDisplay.date(activities[indexPath.row].timestamp)
        cell.modeLabel.text = activities[indexPath.row].isCycling  ? Constant.Constants.cycling : Constant.Constants.jogging
        cell.modeImage.image = (activities[indexPath.row].isCycling ) ? Design.Image.cycling : Design.Image.jogging

        return cell
        
    }
    
    
    
    
}
