//
//  NewActivityViewController.swift
//  Track Pro
//
//  Created by admin on 24/10/20.
//

import UIKit
import CoreLocation
import MapKit
import CoreData

class NewActivityViewController: UIViewController {
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var avgPaceLabel: UILabel!
    
    var isCycling:Bool!
    private var commits = [Activities]()
    private var activity: Activities?
    private let locationManager = LocationManager.shared
    private var seconds = 0
    private var timer: Timer?
    private var distance = Measurement(value: 0, unit: UnitLength.meters)
    private var locationList: [CLLocation] = []
    private var isPaused = false
    private var last:CLLocation?
    private var unit = Unit(rawValue: Constant.Constants.kph)
    
    private let max = 60
    private var speeds = Array<Float>()
    private var avgSpeeds:Double = 0.0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
            self.startActivity()
    }
    
    //MARK:- invalidate everythig started
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        locationManager.stopUpdatingLocation()
        resetUI()

    }
    
    //MARK:- Button Actions
    @IBAction func startActivity(_ sender:UIButton){
        self.isPaused.toggle()
        if isPaused{
            self.startButton.setTitle("RESUME", for: .normal)
            self.startButton.setTitleColor(Design.Color.Primary.darkBlue, for: .normal)
            self.startButton.backgroundColor = Design.Color.Primary.white
            self.startButton.borderWidth = 1
            self.startButton.borderColor = Design.Color.Primary.darkBlue
            self.startButton.tintColor = Design.Color.Primary.darkBlue
            self.startButton.setImage(Design.Image.resume, for: .normal)
        }else{
            self.startButton.setTitle("PAUSE", for: .normal)
            self.startButton.setTitleColor(Design.Color.Primary.white, for: .normal)
            self.startButton.backgroundColor = Design.Color.Primary.darkBlue
            self.startButton.borderWidth = 1
            self.startButton.borderColor = Design.Color.Primary.white
            self.startButton.setImage(Design.Image.pause, for: .normal)
            self.startButton.tintColor = Design.Color.Primary.white


        }
        
    }
    
    @IBAction func stopActivity(_ sender:UIButton){
        
        self.stopActivity()
        
    }
    
    
    //MARK:- Start Activity
    private func startActivity() {
        
        seconds = 0
        distance = Measurement(value: 0, unit: UnitLength.meters)
        locationList.removeAll()
        updateDisplay()
        startLocationUpdates()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            self.eachSecond()
        }
    }
    
    //MARK:- Stop Activity
    private func stopActivity() {
        
        saveActivity()
        timer?.invalidate()
        locationManager.stopUpdatingLocation()

        if isFirstActivity(){
            self.showAlertWithAction(with: Constant.Messages.alertTitile, message: Constant.Messages.alertFirstGoal) {
                self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
            }

        }else{
            self.showAlertWithAction(with: Constant.Messages.alertTitile, message: Constant.Messages.alertElseGoal) {
                self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
            }
        }

    }
    
    func isFirstActivity() -> Bool {
        let request: NSFetchRequest<Activities> = Activities.fetchRequest()

        do {
            commits = try CoreDataStack.context.fetch(request)
            print("Got \(commits.count) commits")
            if commits.count == 1{
                return true
            }else{
                return false
            }
        } catch {
            print("Fetch failed")
            return false
        }
    }
    
    private func resetUI(){
        
        distanceLabel.text = "0 km"
        timeLabel.text = "00:00:00"
        paceLabel.text = "0"
        avgPaceLabel.text = "00 kph"
    }
    
    func eachSecond() {
        if !isPaused{
            seconds += 1
            updateDisplay()
        }
    }
    
    //MARK:- Update display using timer
    private func updateDisplay() {
        let formattedTime = FormatDisplay.time(seconds)
        timeLabel.text = "\(formattedTime)"

        
    }
    
    //MARK:- Start location update
    private func startLocationUpdates() {
        locationManager.delegate = self
        locationManager.activityType = .fitness
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
    }
    
    func findAvgSpeed(){
        
    }
    
    //MARK:- Handle DB
    private func saveActivity() {
        let newActivity = Activities(context: CoreDataStack.context)
        newActivity.distance = FormatDisplay.distance(distance)
        newActivity.duration = Int16(seconds)
        newActivity.timestamp = Date()
        newActivity.avgSpeed = String(format: "%.0f", self.avgSpeeds)
        newActivity.isCycling = isCycling

        CoreDataStack.saveContext()
        NotificationCenter.default.post(name: Notification.Name(Constant.identifiers.dbUpdateObserver), object: nil)

        activity = newActivity
    }
    
    
}

// MARK: - Location Manager Delegate

extension NewActivityViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for newLocation in locations {
            
            
            var total: Float = 0.0
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            
            
            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
            if let lastLocation = locationList.last {
                let delta = newLocation.distance(from: lastLocation)
                distance = distance + Measurement(value: delta, unit: UnitLength.meters)
            }
            locationList.append(newLocation)

            guard let speed = manager.location?.speed else { return }
            
            
            if speeds.count == max {
                speeds.remove(at: 0)
            }
            speeds.append(self.unit!.calculateSpeedInFloat(for: speed))
            speeds.forEach { speed in
                total += speed
            }
            
            
            self.avgSpeeds = Double(total / Float(self.speeds.count))
            self.paceLabel.text = self.unit?.calculateSpeed(for: speed)
            self.avgPaceLabel.text = String(format: "%.0f", self.avgSpeeds) + " kph"
            self.distanceLabel.text = FormatDisplay.distance(distance) 

        }
    }
    

}
