//
//  HomeViewController.swift
//  Track Pro
//
//  Created by admin on 24/10/20.
//

import UIKit
import CoreData

class HomeViewController: UIViewController {
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var avgPaceLabel: UILabel!
    @IBOutlet weak var lastActivityLabel: UILabel!
    @IBOutlet weak var modeImage: UIImageView!
    @IBOutlet weak var recentActivityView: UIView!
    
    let locationManager = LocationManager.shared
    private var activities = [Activities]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchActivitiesFromDB), name: Notification.Name(Constant.identifiers.dbUpdateObserver), object: nil)
        self.fetchActivitiesFromDB()
        
    }
    @IBAction func startActivity(_ sender:UIButton){
        
        if locationManager.authorizationStatus == .authorizedAlways || locationManager.authorizationStatus == .authorizedWhenInUse {
            
            self.showAlertWithChooseAction(with: Constant.Messages.alertTitile, message: Constant.Messages.alertChooseMood) {
                self.presentVC(isCycling: true)
            } Jogging: {
                self.presentVC(isCycling: false)
            }
            
        }else{
            
            self.showDefaultAlert(with: Constant.Messages.alertLocationTitle, message: Constant.Messages.alertLocationMessage)
        }
        
        
        
    }
    
    func initUI(){
        if let activity = activities.last {
            distanceLabel.text =  activity.distance! 
            timeLabel.text = FormatDisplay.time(Int(activity.duration) )
            avgPaceLabel.text = activity.avgSpeed! + "kph"
            self.lastActivityLabel.text = "Last Activity:"+FormatDisplay.date(activity.timestamp)
            modeImage.image = (activity.isCycling ) ? Design.Image.cycling : Design.Image.jogging
            
        }
        
    }
    
    //MARK:- Fetch from DB
    @objc func fetchActivitiesFromDB() {
        let request: NSFetchRequest<Activities> = Activities.fetchRequest()
        let sort = NSSortDescriptor(key: "timestamp", ascending: true)
        request.sortDescriptors = [sort]

        do {
            activities = try CoreDataStack.context.fetch(request)
            print("in home i got \(activities.count) activities")
            if activities.count == 0 {
                self.recentActivityView.isHidden = false
            }else{
                self.recentActivityView.isHidden = true
                self.initUI()
            }
        } catch {
            print("Fetch failed")
        }
    }
    
    
    //MARK:- Present viewcontroller
    func presentVC(isCycling:Bool){
        let vc = self.storyboard?.instantiateViewController(identifier: Constant.identifiers.newActivityVC) as! NewActivityViewController
        vc.isCycling = isCycling
        self.present(vc, animated: true, completion: nil)
    }
    
    
}
