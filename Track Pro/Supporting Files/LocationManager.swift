//
//  LocationManager.swift
//  Track Pro
//
//  Created by admin on 24/10/20.
//

import Foundation
import CoreLocation

class LocationManager  {
    static let shared = CLLocationManager()
    
    private init() { }
  }
