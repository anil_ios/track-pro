//
//  Extensions.swift
//  Track Pro
//
//  Created by admin on 23/10/20.
//

import Foundation
import UIKit

extension UIViewController{
    //MARK:- ALert in view controller
    
    func showDefaultAlert(with title:String?,message:String?){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithAction(with title:String?,message:String?,OK:@escaping()->()){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            OK()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func showAlertWithChooseAction(with title:String?,message:String?,Cycling:@escaping()->(),Jogging:@escaping()->()){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constant.Constants.cycling, style: .default, handler: { (_) in
            Cycling()
        }))
        
        alert.addAction(UIAlertAction(title: Constant.Constants.jogging, style: .default, handler: { (_) in
            Jogging()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    func showAlertWithRetry(with title:String?,message:String?,action:@escaping()->()){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Retry", style: .destructive, handler: { (_) in
            action()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}

