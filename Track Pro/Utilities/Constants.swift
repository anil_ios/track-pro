//
//  Constants.swift
//  Track Pro
//
//  Created by admin on 24/10/20.
//

import Foundation
import UIKit

struct Design {
    struct Color {
        struct Primary {
             static let darkBlue = #colorLiteral(red: 0.1025191024, green: 0.223176986, blue: 0.371999681, alpha: 1)
             static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

           
        }
        struct Secondary {
           
        }
        struct Grayscale {
            
        }
    }
    struct Image {
        static let pause = UIImage(systemName: "pause.fill")
        static let resume = UIImage(systemName: "play.fill")
        static let cycling = UIImage(named: "Cycle")
        static let jogging = UIImage(named: "Run")
        static let cyclingBG = UIImage(named: "bgCycle")
        static let joggingBG = UIImage(named: "bgRun")


    }
    struct Font {
       
       
    }
    
}

struct Constant {
    
    struct Constants {
        static let kph =  "km/h"
        static let cycling =  "Cycling"
        static let jogging =  "Jogging"

    }

    struct Messages {
        static let alertTitile = "Track Pro"
        static let alertFirstGoal = " You just kicked off a new life routine"
        static let alertElseGoal = "Well done, Keep going"
        static let alertChooseMood = "Hey,What's your mood today"
        static let alertLocationTitle = "oops!! Something missing"
        static let alertLocationMessage = "Please go to settings and enable location to track your records"


    }
    struct identifiers{
        
        static let dbUpdateObserver = "DBUpdated"
        static let newActivityVC = "NewActivityViewController"
        static let tabBarVC = "tab"
        static let ActivityListCell = "ActivityListCell"

    }
    
}

